package ru.tsc.chertkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskShowListByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowListByProjectIdRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }

}
