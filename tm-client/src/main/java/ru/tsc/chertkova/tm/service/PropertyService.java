package ru.tsc.chertkova.tm.service;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['buildNumber']}")
    public String applicationVersion;

    @Value("#{environment['server.port']}")
    public String port;

    @Value("#{environment['server.host']}")
    public String host;

    @Value("#{environment['developer']}")
    public String authorName;

    @Value("#{environment['email']}")
    public String authorEmail;

}
