package ru.tsc.chertkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractUserListener;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "logout";

    @NotNull
    public static final String DESCRIPTION = "Logout user profile.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[USER LOGOUT]");
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
