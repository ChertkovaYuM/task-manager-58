package ru.tsc.chertkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.user.UserUnlockRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractUserListener;
import ru.tsc.chertkova.tm.util.TerminalUtil;

@Component
public final class UserUnlockListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-unlock";

    @NotNull
    public static final String DESCRIPTION = "Unlock user profile.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userUnlockListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        getUserEndpoint().unlockUser(new UserUnlockRequest(getToken(), login));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
