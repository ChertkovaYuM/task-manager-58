package ru.tsc.chertkova.tm.api.model;

import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @Nullable
    String getName();

    @Nullable
    String getDescription();

    @Nullable
    String getArgument();

}
