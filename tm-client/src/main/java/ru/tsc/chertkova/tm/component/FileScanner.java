package ru.tsc.chertkova.tm.component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.IPropertyService;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractListener;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@AllArgsConstructor
public final class FileScanner {

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    private final File folder = new File("./");

    @NotNull
    private final ScheduledExecutorService es
            = Executors.newSingleThreadScheduledExecutor();

    public void init() {
        start();
    }

    @SneakyThrows
    public void process() {
        @NotNull final List<String> files = Files.list(Paths.get(folder.toString()))
                .filter(file -> !Files.isDirectory(file))
                .map(Path::getFileName)
                .map(Path::toString)
                .collect(Collectors.toList());
        for (String fileName : files) {
            for (@NotNull final AbstractListener listener : listeners) {
                if (fileName.equals(listener.command())) {
                    publisher.publishEvent(new ConsoleEvent(fileName));
                    System.out.println(fileName);
                    Files.delete(Paths.get(fileName));
                }
            }
        }
    }

    private void start() {
        es.scheduleWithFixedDelay(this::process, 0, 3, TimeUnit.SECONDS);
    }

}
